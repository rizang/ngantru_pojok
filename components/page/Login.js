import React, {Component} from 'react';
import {StyleSheet, View, Image, Button, Text, TextInput} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label } from 'native-base';

export default class Login extends Component {
	constructor(props){
		super(props)
		this.state = {
	    	nik: '',
	    	pass: ''
	  	};
  	}

	render(){
		return(
			<View style={styles.container}>
				<Image
			        style={styles.tinyLogo}
			        source={require('../../assets/img/dokumen.png')}
			    />
			    <Form>
		            <Item inlineLabel>
		              <Label>Niks</Label>
		              <Input />
		            </Item>
		            <Item inlineLabel last>
		              <Label>Password</Label>
		              <Input />
		            </Item>
		        </Form>
			    <Button
			        title="Cari Data"
			        onPress={() => Alert.alert('Simple Button pressed')}
			    />
		   	</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 16,
  },
  tinyLogo: {
    width: 150,
    height: 150,
  },
});
